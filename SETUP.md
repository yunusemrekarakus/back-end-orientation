We start installation with Ubuntu 20.04.2 system.
You can use other Linux distros if you want but you will have to figure out issues yourself.
In Ubuntu, even version values are [LTS](https://wiki.ubuntu.com/LTS). So it is recommended to install those(16, 18, 20 etc.).
Currently most recent LTS version is 20.04.2.
This document will _probably_ work for other versions of Ubuntu with or without small differences.

```shell
osboxes@osboxes:~$ lsb_release -a
No LSB modules are available.
Distributor ID:	Ubuntu
Description:	Ubuntu 20.04.2 LTS
Release:	20.04
Codename:	focal
```
It is recommended to upgrade packages first. `-y` auto accepts the continue prompt. `apt update` updates local available version list and `apt upgrade` tries to install those versions.
This will take time, you might wanna grab a cup of coffee. :)
```shell
sudo apt update -y
sudo apt upgrade -y
```
Omnitron project requires Python 2.7 but we are trying to upgrade the system to Python 3.7, so we will need to install both versions.
Ubuntu 20.04 has Python 3.8 by default. Python 2.7 can be installed easily with `apt install`.
```shell
sudo apt install python2
```
However, Python 3.7 is not listed in apt. We need to update apt lists to get it.
```shell
sudo add-apt-repository ppa:deadsnakes/ppa
sudo apt update -y
sudo apt install python3.7
```
We have necessary Python versions but we also need package manager `pip` for both of them.
```shell
sudo apt install python3-pip
sudo apt install curl
curl https://bootstrap.pypa.io/pip/2.7/get-pip.py --output get-pip.py
sudo python2 get-pip.py
```
To verify the installation, we can try `--version` commands.
```shell
osboxes@osboxes:~$ pip2 --version
pip 20.3.4 from /usr/local/lib/python2.7/dist-packages/pip (python 2.7)
osboxes@osboxes:~$ pip3 --version
pip 20.0.2 from /usr/lib/python3/dist-packages/pip (python 3.8)
```
Upgrade pip versions to eliminate any issues we may encounter later on.
```shell
pip2 install --upgrade pip
pip3 install --upgrade pip
```
Just copy paste the command below and don't ask questions. We need them here and there. :)) 
(Postgres binaries, some libraries required for package dependencies, Celery, Vim, Git, Redis)
```shell
sudo apt install postgresql postgresql-contrib git libpq-dev libjpeg8-dev redis-server python3.7-dev python2.7-dev vim python-celery-common
```

OK! Now we are done with the basic installations. Let's get the codes from Bitbucket. To do that, we first need to authorize our computer
by adding SSH public key to Bitbucket. Create the key with `ssh-keygen` and copy the result of `cat` command. 
Find `Personal Settings/Security/SSH Keys` menu by clicking your avatar on Bitbucket and paste the result there. 
```shell
osboxes@osboxes:~$ ssh-keygen
Generating public/private rsa key pair.
Enter file in which to save the key (/home/osboxes/.ssh/id_rsa): 
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in /home/osboxes/.ssh/id_rsa
Your public key has been saved in /home/osboxes/.ssh/id_rsa.pub
The key fingerprint is:
SHA256:25c2hMf7PWJVTNAYznE6bbyPqZal8Gzc9EjunbbIt3A osboxes@osboxes
The key's randomart image is:
+---[RSA 3072]----+
|              +=.|
|             o.*o|
|              =o+|
|           o   o+|
|        S . +  ..|
|         o.o o++.|
|        . .=*X+E.|
|           oX*B*o|
|           oo+*+*|
+----[SHA256]-----+
osboxes@osboxes:~$ cat ~/.ssh/id_rsa.pub
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC8tUmVaNJQZAdOvkFT2op81zzHkR6DVyxY4UcuygHaOJlsPfbBRVdkf/AqsrmC/O0S6m45xwVQwl0PVh7aK/Y9lsHnWAiugYaqsFPy7PR3lginiLsghcCez9ld062bURXMlr1YAKM7u4HBmr8m93sVIjCoiC88GDVkWzFuR74oiBFWHhnsuacLEdCOofI4rMRM/re4LbF9aXwmm5/+UUoiZTx/OKYPBkKazhwAM0hyEMaAwVrxk1kv8g72eMIYGPN1vrvM2RSOIbITZavz7UtsMT6peMPwppfm41oMzFwLpFflzjQXzdY8ujRTeEyOOvZPLaW3NQPhJP4NPpGOoJbChpOb3LapaCEblh/chJmtMFaATawIMqLom+zH0irKLPjx9Cp3pK5paXLUETSIIv33wy0y/15reLsN5vcyx3kNEGrZ7XRHvM+ijUxJTd1e0gNMLsNIZk6n8S5Sc1mZN6IBDXeHD3Cok5nTkLIeMQtzg58G2gZyMryFmpBcBJsGtBk= osboxes@osboxes
```
Go to project directory and clone the project.
```shell
osboxes@osboxes:~$ cd <my_project_directory>
osboxes@osboxes:~$ git clone git@bitbucket.org:akinonteam/omnitron_backend.git
Cloning into 'omnitron_backend'...
remote: Enumerating objects: 184550, done.
remote: Counting objects: 100% (4164/4164), done.
remote: Compressing objects: 100% (1128/1128), done.
remote: Total 184550 (delta 3768), reused 3115 (delta 3034), pack-reused 180386
Receiving objects: 100% (184550/184550), 142.76 MiB | 1.99 MiB/s, done.
Resolving deltas: 100% (135728/135728), done.
```
Install `virtualenvwrapper` to manage virtual environments.
It places the environments under home directory `$WORKON_HOME` we will define below. 
We can activate them wherever needed.

Copy the output of `which` command as we will paste them to `.bashrc` file in the next step.
```shell
osboxes@osboxes:~$ pip3 install virtualenvwrapper
osboxes@osboxes:~$ which virtualenvwrapper.sh
/home/osboxes/.local/bin/virtualenvwrapper.sh
```
Open the file with your favourite text editor `vim ~/.bashrc`. You can use `gedit ~/.bashrc` as well. 
Just make sure to save the changes before closing. Add the lines below to the end of `.bashrc` file.
```text
export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3
export WORKON_HOME=~/Envs
source /home/osboxes/.local/bin/virtualenvwrapper.sh
```
Now if you do `source ~/.bashrc`, virtualenvwrapper will be ready to use and no further configuration needed.
Short demo of its usage and creation of necessary environments.
```shell
osboxes@osboxes:~$ mkvirtualenv deneme
created virtual environment CPython3.8.10.final.0-64 in 140ms
    ...
(deneme) osboxes@osboxes:~$ deactivate
osboxes@osboxes:~$ which python2.7
/usr/bin/python2.7
osboxes@osboxes:~$ python2 --version
Python 2.7.18
osboxes@osboxes:~$ mkvirtualenv -p /usr/bin/python2.7 omni
created virtual environment CPython2.7.18.final.0-64 in 769ms
    ...
(omni) osboxes@osboxes:~$ lsvirtualenv
deneme
======

omni
====

(omni) osboxes@osboxes:~$ workon deneme
(deneme) osboxes@osboxes:~$ workon omni
(omni) osboxes@osboxes:~$ rmvirtualenv deneme
Removing deneme...
(omni) osboxes@osboxes:~$ mkvirtualenv -p /usr/bin/python3.7 omni3
created virtual environment CPython3.7.11.final.0-64 in 966ms
    ...

```
OK! Now go to the project directory, activate python 2.7 environment `omni` and install python packages.
```shell
(omni) osboxes@osboxes:~/omnitron_backend$ pip install -r requirements.txt
```
Omnicore project is defined as a submodule, so we must also get them from Bitbucket with:
```shell
git submodule update --init --recursive
```
Now if you try to run the application, you will get a peer authentication error. 
To handle that, we need to update `pg_hba.conf` configuration file. It is located on
`/etc/postgresql/12/main/pg_hba.conf` in my system, you can `find` it as shown below. Stop the command with `Ctrl+C` once it shows a result
otherwise it will search the whole system.
```shell
(omni) osboxes@osboxes:~/omnitron_backend$ find / -name "pg_hba.conf" 2>/dev/null
/etc/postgresql/12/main/pg_hba.conf
^C
```
Edit the file `sudo vim /etc/postgresql/12/main/pg_hba.conf` and change `peer` with `trust` as shown below.
Also dont forget to add the referred line.

Postgres user will not require a password to make a database connection.
You can also use `md5` but, you will have to set a password for user postgres and specify that in Django settings file.

```text
local all postgres peer  #find this line  
local all postgres trust #change peer with trust

#AND

host all postgres 127.0.0.1/32 trust #add this line BEFORE the line below
host all all 127.0.0.1/32 md5
```
Now we need to restart postgres daemon to apply the changes. `12` or `main` might be different for your case.
Correct values should be the ones used in `pg_hba.conf` file path.
```shell
sudo systemctl restart postgresql@12-main
```
We need to create the databases and import initial dump. Ask your buddy about dump file.
```shell
(omni) osboxes@osboxes:~/omnitron_backend$ sudo -i -u postgres
postgres@osboxes:~$ createdb omni
postgres@osboxes:~$ createdb omnishop
postgres@osboxes:~$ cd /home/osboxes/
postgres@osboxes:/home/osboxes$ psql omni < omni_clean.txt 
postgres@osboxes:/home/osboxes$ exit
(omni) osboxes@osboxes:~/omnitron_backend$
```
If we run the application at this stage, it will work but services will mostly fail because migrations are not applied.
```shell
(omni) osboxes@osboxes:~/omnitron_backend$ python manage.py migrate
(omni) osboxes@osboxes:~/omnitron_backend$ python manage.py runserver
```
To run the celery tasks, open another terminal in project directory with environment activated
```shell
celery -A omnipro worker -l info --beat
```
Run the tests with
```shell
python manage.py test omnitron --settings=omnipro.settings_test_pipeline --verbosity 2 --keepdb
```

Try to run the login query, if you get a token in response you already have a user defined in your initial dump file.
If you get an error, follow the next step to create a new user.

```shell
curl --location --request POST 'http://localhost:8000/api/v1/auth/login/' --header 'Content-Type: application/x-www-form-urlencoded' --data-urlencode 'username=admin' --data-urlencode 'password=password123'
curl --location --request GET 'http://localhost:8000/api/v1/channels/' --header 'Content-Type: application/json' --header 'Authorization: Token 3084d6a5aad144cc8052e231cbf75f146110bc35'
```

You can create a super user with following command. It returns simple true/false response indicating whether user is created or not. 
If it is not created, there is probably a superuser different from the one we entered in the login curl command above.  

```shell
python manage.py setup_root_superuser 
--username=username
--password=password
--email=test@akinon.com
--firstname=name
--lastname=lastname
```

You are ready to go!!

```
^[:wq
```
