# Backend Orientation Akinon
---
#### Veritabanlarini Olustur


```bash
sudo -u postgres createdb omnitron
```

```bash
sudo -u postgres createdb omnishop
```

- Aslinda default veritabanlari omnishop ve omni.
- Sadece bunlari olusturup kodda bir degisiklik yapmadan migrationlar yapilabilir.
- Fakat ayri veritabaniyla calisilmak istenirse farkli veritabani olusturulup,
asagidaki adimlar takip edilir.

---
#### Veritabanlarini Django'nun gorecegi sekilde ayarla


- shomnipro ve omnitron_project/omnipro altına settings_local.py diye bir dosya oluştur
- Icerisinde olusturdugun dbleri asagidaki gibi tanimla
    - omnipro
    ```bash
    DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'omnitron', # ADINI ne verdiysen
        'USER': 'postgres',
        'PASSWORD': '1234',
        'HOST': '127.0.0.1',
        'PORT': '5432'
        }
    }
    ```
    - shomnipro

    ```bash
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql',
            'NAME': 'omnishop', # ADINI ne verdiysen
            'USER': 'postgres',
            'PASSWORD': '1234',
            'HOST': '127.0.0.1',
            'PORT': '5432'
            }
        }
    }
    ```

---
#### Migrationlar

- omnishop ve omnitron altinda ayri ayri asagidaki komutlar calistirilicaktir.

```bash
python manage.py makemigrations
```

```bash
python manage.py migrate
```
---
#### Calistirma

```bash
python manage.py runserver # omnishop icin 8080, omnitron default
```

### [EKSTRA] Cache Temizleme
```bash
python manage.py clear_cache
```
---

### Shell Acma (omnishop altinda)
```bash
python manage.py shell_plus --settings=shomnipro.settings_local
```
#### Create Super User
```py
python manage.py createsupersuper
```
---

#### ShippingOption

##### Shipping rules incele -> omnitron_project.omnishop.shippings.rules
```py
shipping = ShippingOption()
shipping.name = 'Yurtici'
shipping.slug = 'yurtici'
shipping.rule = {
  "children": [
    {
      "name": "Any Rule",
      "slug": "any-rule"
    },
    {
      "child": {
        "name": "Retail Store Rule",
        "slug": "retail-store-rule"
      },
      "name": "Not Rule",
      "slug": "not-rule"
    }
  ],
  "name": "And Rule",
  "slug": "and-rule"
}
shipping.calculator = {
    "base_shipping": "fixed-price-calculator",
    "fixed_price": "9.99",
    "name": "Discount Based Calculator",
    "slug": "discount-based-calculator"
}
shipping.save()
```
---

### Shell Acma (omnitron project altinda)
```py
python manage.py shell_plus
```

### CategoryNodeService
```py
from omnitron.catalogs.service import CategoryNodeService
ct_service = CategoryNodeService()
ct_root = ct_service.create_category_root_node(name="Shop")
ct_1 = ct_service._create_category_child_node(node=ct_root.id, name="Erkek")
ct_1_1 = ct_service._create_category_child_node(node=ct_1.id, name="Ayakkabi")
```

### CategoryTree
```py
category_tree = CategoryTree()
category_tree.name='Shop'
category_tree.category_root = ct_root
category_tree.save()
```

### PriceList
```py
price_list = PriceList()
price_list.name = 'default_price_list'
price_list.code = price_list.name
price_list.save()
```

### StockList
```py
stock_list = StockList()
stock_list.name = 'default_stock_list'
stock_list.code = stock_list.name
stock_list.save()
```

### Catalog
```py
catalog = Catalog()
catalog.name = 'ShopCatalog'
catalog.stock_list = stock_list
catalog.price_list = price_list
catalog.category_tree = category_tree
catalog.priority_list = None
catalog.save()
```


### Channel

- Shop web sitesi bir satis kanali ve biz bunu omnitrona gosterecegiz. Channel olarak ekle.

```py
from omnitron.channels.enums import ChannelType
channel = Channel()
channel.name = 'Shop'
channel.channel_type = ChannelType.web
channel.catalog = catalog
channel.conf = {
    'username': 'admin',
    'password': 'password123',
    'server': 'http://127.0.0.1:8080/api/v1/',
    'sales_url': 'http://127.0.0.1:8080'
}
channel.save()
```

### Attribute (beden, urun, vs...)
```py
from omnicore.products.enums import AttributeInputTypes
attribute = Attribute()
attribute.key = 'Icerik'
attribute.data_type = AttributeInputTypes.dropdown
attribute.erp_code = 'erp_icerik'
attribute.is_required = False
attribute.is_searchable = False
attribute.is_variant = False
attribute.is_visible = False
attribute.name = 'Icerik'
attribute.entity_type_id = 1
attribute.translations = {}
attribute.pre_attribute = False
attribute.is_filterable = False
attribute.is_form_required = False
attribute.is_form_field_required = False
attribute.save()
```

### AttributeValue (Attribute teki Key lerin Value leri)
```py
attribute_value = AttributeValue()
attribute_value.value = '42'
attribute_value.erp_code = '42'
attribute_value.attribute = attribute
attribute_value.save()

attribute_value = AttributeValue()
attribute_value.value = '43'
attribute_value.erp_code = '43'
attribute_value.attribute = attribute
attribute_value.save()

attribute_value = AttributeValue()
attribute_value.value = '44'
attribute_value.erp_code = '44'
attribute_value.attribute = attribute
attribute_value.save()
```


### AttributeSet
- (Attribute -> Beden, AttributeValue = S, M, AttributeSet -> Giyim)
```py
from omnicore.products.enums import AttributeSetTypes
attribute_set = AttributeSet()
attribute_set.name = 'Giyim'
attribute_set.set_type = AttributeSetTypes.simple
attribute_set.save()
```

### AttributeConfig
- (numarayi ve giyimi yani Seti birbirine baglayacaz)
```py
attribute_config = AttributeConfig()
attribute_config.attribute = attribute
attribute_config.attribute_set = attribute_set
attribute_config.is_searchable = True
attribute_config.is_variant = True
attribute_config.is_localizable = False
attribute_config.is_filterable = True
attribute_config.is_form_required = False
attribute_config.is_form_field_required = False
attribute_config.save()
```


### Product
- (sku => stock kodu - stock keeping unit)
```py
from omnicore.products.enums import ProductTypes
product = Product()
product.name = 'Erkek Ayakkabi'
product.base_code = '35255'
product.sku = '12349876'
product.product_type = ProductTypes.simple
product.is_active = True
product.attribute = {'Icerik':'43'}
product.name = 'Erkek Timberland Ayakkabi'
product.extra_attributes = {}
product.attribute_set = attribute_set
product.localized_attributes = {}
product.attributes_kwargs = {'Icerik': {'label':'43', 'value':'43', 'data_type':'dropdown', 'translations':None}}
product.listing_code = 123123
product.save()
```

### ProductPrice
```py
product_price = ProductPrice()
product_price.product = product
from decimal import Decimal
product_price.price = Decimal(10)
product_price.price_list = price_list
product_price.tax_rate = '18.0'
product_price.discount_percentage='0'
product_price.save()
```


### ProductStock
```py
product_stock = ProductStock()
product_stock.product = product
product_stock.stock_list = stock_list
product_stock.stock = 10000
product_stock.save()
```

### CatalogItem
- (Catalog ile Product baglayacaz)
```py
catalog_item = CatalogItem()
catalog_item.product = product
catalog_item.catalog = catalog
catalog_item.save()
```


### ProductCategory
- (Category ile Product baglanacak)
```py
product_category = ProductCategory()
product_category.product = product
product_category.category = ct_1_1
product_category.save()
```

### CargoCompany
```py
cargo_company = CargoCompany()
cargo_company.name = 'UPS'
cargo_company.erp_code = 'ups'
cargo_company.shipping_company = 'ups'
cargo_company.save()

cargo_company = CargoCompany()
cargo_company.name = 'Yurtici Kargo'
cargo_company.erp_code = 'yurtici'
cargo_company.shipping_company = 'yurtici'
cargo_company.save()

cargo_company = CargoCompany()
cargo_company.name = 'MNG Kargo'
cargo_company.erp_code = 'mng'
cargo_company.shipping_company = 'mng'
cargo_company.save()
```

### CancellationReason
```py
from omnitron.orders.enums import CancellationPlanType
cancellation_reason = CancellationReason()
cancellation_reason.subject = 'Ucuzunu buldum'
cancellation_reason.cancellation_type = CancellationPlanType.cancel
cancellation_reason.send_to_remote = False
cancellation_reason.save()

cancellation_reason = CancellationReason()
cancellation_reason.subject = 'Bedeni Uymadi'
cancellation_reason.cancellation_type = CancellationPlanType.cancel
cancellation_reason.send_to_remote = False
cancellation_reason.save()
```

### Country, City, Township, District
```py
country = Country()
country.code = 'tr'
country.name = 'Turkiye'
country.save()
city = City()
city.name = 'Istanbul'
city.country = country
city.save()
township = Township()
township.name = 'Gazi Osman Pasa'
township.city = city
township.save()
district = District()
district.city = city
district.township = township
district.name = 'Yenimahalle'
district.save()
```


### Payment
```py
card_names = {
        'advantage': 'advantage.png',
        'axess': 'axess.png',
        'bonus': 'bonus.png',
        'cardfinans': 'cardfinans.png',
        'diger': 'diger.png',
        'maximum': 'maximum.png',
        'world': 'world.png',
    }
from omnicore.payments.enums import CardPaymentType
card_types = {
        'CREDIT_CARD': CardPaymentType.credit.value,
        'DEBIT_CARD': CardPaymentType.debit.value,
    }
import json
import urllib
import os
bin_number_url = """https://gist.githubusercontent.com/aykut/7dd2f5f2469eadb30488eb1f5a46e7a2/raw/0773b681bfabc44bec4fbfbffed4566f9b205f8a/bin_numbers.json"""
urllib.urlopen(bin_number_url)
bin_numbers = urllib.urlopen(bin_number_url)
bin_numbers = json.load(bin_numbers)
from django.conf import settings
card_folder = os.path.join(settings.BASE_DIR, 'static_omnishop', 'assets/img/card-logo/')
```

### Pos
```py
from omnicore.payments.enums import GatewayChoices
pos = Pos()
pos.slug = 'nestpay'
pos.gateway = GatewayChoices.nestpay
pos.name = 'NestPay'
pos.resource_url = 'https://entegrasyon.asseco-see.com.tr/fim/api'
pos.config = { u'auth': {
                    u'client_id': u'700655000100',
                    u'password': u'ISBANK07',
                    u'username': u'ISBANKAPI',
                }}
pos.channel = channel
pos.save()
```
```py
from django.core.files.uploadedfile import SimpleUploadedFile
from django.utils.text import slugify

for bin_number in bin_numbers:
    if BinNumber.objects.filter(
            bin_number=bin_number['binNumber'], channel=channel).exists():
        logger.debug('%s already exists.' % bin_number['binNumber'])
        continue
    bank, _ = Bank.objects.get_or_create(
        slug=slugify(bin_number['bankName']),
        defaults={'name': bin_number['bankName']},
        channel=channel
    )
    card_type_slug = slugify(bin_number['cardFamily'])
    if card_type_slug == 'banko-card' and card_types[
        bin_number['cardType']] == 'debit':
        card_type_slug = 'banko-card-debit'
    card_logo = os.path.join(card_folder,
                             card_names.get(card_type_slug, 'diger.png'))
    card_type, _ = CardType.objects.get_or_create(
        slug=card_type_slug,
        channel=channel,
        defaults={
            'name': bin_number['cardFamily'],
            'logo': SimpleUploadedFile('a.png',
                                       open(card_logo, 'rb').read()),
        }
    )
    cpt = card_types[bin_number['cardType']]
    card, created = Card.objects.get_or_create(
        bank=bank, card_type=card_type, card_payment_type=cpt,
        channel=channel,
        defaults={'cash_payment': pos, 'installment_payment': pos,
                  'name': bin_number['cardFamily'], 'slug': card_type_slug}
    )
    BinNumber.objects.create(
        bin_number=bin_number['binNumber'], card=card, channel=channel)
    if created:
        Installment.objects.create(
            card=card, installment_count=1, label=u'Peşin',
            interest_rate='0.00')
```

- bin numarasi verip Banka olusturuyoruz.
- CardType olusturduk ve slug verip hangi channelda tanimli olacagini verdik.
- Card objesi olusturduk ve hangi bankaya ait oldugunu kart tipinin ne oldugunu ve payment methodunun ne oldugunu setledik.
- Bin numarasi olusturduk.
- Bu bin numarasi da card objesini ve channeli aliyor.
- Installment olusturduk. Taksit var mi kac taksit?

---

### Bank
```py
bank = Bank()
bank.name = 'Other'
bank.slug = 'other'
card_logo = os.path.join(card_folder, 'diger.png')
data = open(card_logo, 'rb').read()
bank.logo = SimpleUploadedFile('b.png', data)
bank.channel = channel
bank.save()
```

### CardType
```py
card_type = CardType()
card_type.name = 'Other'
card_type.slug = 'other'
card_type.logo = bank.logo
card_type.channel = channel
card_type.save()
```

### Card
```py
card = Card()
card.name = 'Other'
card.slug = 'other'
card.card_type = card_type
card.bank = bank
card.cash_payment = pos
card.installment_payment = pos
card.save()
```

### Installment(Taksit)
```py
installment = Installment()
installment.card = card
installment.is_active = True
installment.installment_count = 1
installment.label = 'pesin'
installment.interest_rate = '0.0'
installment.save()
```

### PaymentOption
```py
from omnicore.payments.enums import PaymentType
payment_option = PaymentOption()
payment_option.channel = channel
payment_option.slug = 'credit_card'
payment_option.name= 'kredi karti'
payment_option.payment_type = PaymentType.credit_card
payment_option.is_active = True
payment_option.config = {}
payment_option.sort_order = 0
payment_option.save()

payment_option = PaymentOption()
payment_option.slug = 'cash_register'
payment_option.name= 'Cash Register'
payment_option.payment_type = PaymentType.cash_register
payment_option.is_active = True
payment_option.channel = channel
payment_option.config = {}
payment_option.sort_order = 1
payment_option.save()

payment_option = PaymentOption()
payment_option.channel = channel
payment_option.slug = 'pay_on_delivery'
payment_option.name= 'Kapida Odeme'
payment_option.payment_type = PaymentType.pay_on_delivery
payment_option.is_active = True
payment_option.config = {}
payment_option.sort_order = 0
payment_option.save()
```
---
## Omnitron to Omnishop Cases

### Shop tarafindan User olusturma
> Oluşturduğumuz channel'ın conf'una girdiğimiz bilgilere göre oluşturmamız gerekiyor.
> **username**='admin',  **password**='password12', **email**='admin@mail.com' (geçerli formatta herhangi bir mail)
```py
python manage.py createsupersuper --settings=shomnipro.settings
```

### Shop tarafinda indexlenebilir Product lar sorgulaniyor
```py
Product.objects.filter(is_listable=True,
                       is_active=True,
                       productstock__stock__gt=0,
                       productprice__price__gt=0,
                       productcategory__isnull=False)
```

### Indexing
- (Listelenebilir olan, aktif olan, stogu olan, price'i olan ve kategorisi olan -> bunlarin hepsi varsa urun indexlenebilir demektir)
- Urunle ilgili hersey var fakat image yok.
- Image olusturup aramada cikmasi icin ElasticSearch'e aktaracagiz.
- Image'i Panel tarafindan olusturacaz.

```bash
git clone git@bitbucket.org:akinonteam/omnitron_frontend.git
cd omnitron_frontend
virtualenv venv -p python2 && source venv/bin/activate
```
- Daha sonra omnitronun frontend kurulumunu yapacagiz
```bash
pip install nodeenv;nodeenv -p --prebuilt
```
- pip uzerinden nodejs kurduk simdi paketlerimizi alalim
```bash
npm install -g gulp-cli bower
npm install
bower install
```
- simdi ayaga kaldiralim
```bash
gulp serve
```
- Simdi omnitronun arayuzune 8083 portundan gidip giris yapin
- sonrasinda resim eklemek icin urunler>urun havuzuna gidin
- oradan urunu secin ve resim ekleyip zorunlu alanlari doldurun
- ardindan kayit butonuna basin.
- Artik urunumuz hazir ancak henuz shopta gorunmuyor.
- Bu sebepten oturu shop tarafina gondermemiz lazim.
- Parca parca bilgilerini gonderecegiz.


### Omnishop İçin Dataları Omnitrondan Gönderme

- (Şart değil)Omnitron tarafinda login olduk other kartina bin numarasi ekledik -> 545454
```py
from omnitron.channels.integrations.omniweb.tasks import *
# channel_id=1 (shop channel id si ne ise)
send_products(1)
send_product_image(1)  # bu adim icin resmi eklemis olmak gerekiyor
send_product_stock(1)
send_product_price(1)
send_all(1)
send_payment(1)
```
- kodu calistirdik.
- Simdi urunu indexleyelim ve urun gorunsun.
```bash
python manage.py index_products --settings=shomnipro.settings_local # -> Icerigine bak. Indekslenebilir olanlari indeksliyor.
```

### Siparis Verme

- Frontend tarafindan bir siparis olusturduk. Bir urun satin aldik aslinda.
- Eklenen bu siparis omnitron tarafinda gozukmuyor.
- Gozukmesi icin (Omnitron tarafinda):

```py
from omnitron.channels.integrations.omniweb.tasks import get_orders
get_orders(channel.id)
`````

### Test Kartları

```
4022774022774026
12/23 000
```
https://entegrasyon.paratika.com.tr/paratika/api/v2/doc#test-cards